package com.example.ecommerce.DrawerNavView

import android.util.Log
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.ecommerce.DataClass.DrawerMenu1
import kotlinx.android.synthetic.main.drawer_menu_item.view.*

class NavigationMenuAdapter(var data: List<DrawerMenu1>) : RecyclerView.Adapter<NavigationMenuViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NavigationMenuViewHolder =
        NavigationMenuViewHolder(parent)

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: NavigationMenuViewHolder, position: Int) {
        val data = this.data[position]
        holder.itemView.setOnClickListener(data.listener)
        holder.itemView.menu1_name.text = data.name
    }

}
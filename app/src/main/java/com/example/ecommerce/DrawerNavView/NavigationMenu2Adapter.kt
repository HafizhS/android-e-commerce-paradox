package com.example.ecommerce.DrawerNavView

import android.view.ViewGroup
import androidx.core.view.GravityCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.ecommerce.DataClass.DrawerMenu2
import com.example.ecommerce.DataClass.Kategori
import com.example.ecommerce.ExploreBarang.ExploreBarangFragment
import com.example.ecommerce.MainActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.drawer_menu2_item.view.*

class NavigationMenu2Adapter(val data: List<Kategori>, var drawerMenu2: DrawerMenu2) :
    RecyclerView.Adapter<NavigationMenu2ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NavigationMenu2ViewHolder =
        NavigationMenu2ViewHolder(parent)

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: NavigationMenu2ViewHolder, position: Int) {
        val data = this.data[position]
        drawerMenu2.category2 = data.nama_kategori
        holder.itemView.menu2_text.text = data.nama_kategori.capitalize()
        holder.itemView.setOnClickListener {
            val manager = holder.itemView.context as MainActivity
            manager.drawer_layout.closeDrawer(GravityCompat.START)
            manager.replaceFragmentToStack(ExploreBarangFragment(drawerMenu2.category1, data.nama_kategori), "")
        }
    }

}
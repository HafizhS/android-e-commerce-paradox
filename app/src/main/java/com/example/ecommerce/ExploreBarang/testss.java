package com.example.ecommerce.ExploreBarang;

import com.google.gson.annotations.SerializedName;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

interface testss {


    @GET("artikel")
    Call<Artikel> ArtikelRequest(@Header("Authorization") String token, @Query("page") int noPage);


}

class Artikel {

    @SerializedName("id")
    public int id;

    @SerializedName("title")
    public String title;

    @SerializedName("description")
    public String desc;

    @SerializedName("users_id")
    public int usersId;

    @SerializedName("created_at")
    public String createdAt;

    @SerializedName("updated_at")
    public String updatedAt;

}

package com.example.ecommerce.ExploreBarang

import android.content.Context
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.ecommerce.Cipher.decode
import com.example.ecommerce.DataClass.Barang
import com.example.ecommerce.DetailBarangFragment
import com.example.ecommerce.MainActivity
import kotlinx.android.synthetic.main.barang_item.view.*

class ExploreBarangAdapter(val activity: MainActivity, val context: Context, val data: List<Barang>) :
    RecyclerView.Adapter<ExploreBarangViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExploreBarangViewHolder {
        return ExploreBarangViewHolder(parent)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: ExploreBarangViewHolder, position: Int) {
        val data = this.data[position]
        holder.itemView.exploreitemNamabarang.text = data.name
        holder.itemView.exploreitemHargabarang.text = "Rp. ${activity.currencyFormat(data.harga)}"
        Glide.with(context).load(decode(data.gambar)).into(holder.itemView.exploreitemGambaramabarang)
        holder.itemView.setOnClickListener {
            activity.replaceFragmentToStack(DetailBarangFragment(data), "")
        }
    }

}
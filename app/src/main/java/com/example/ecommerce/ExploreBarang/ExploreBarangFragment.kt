package com.example.ecommerce.ExploreBarang

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.ActionBar
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.GridLayoutManager
import com.example.ecommerce.*
import com.example.ecommerce.DataClass.Barang
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.explore_barang_fragment.*
import retrofit2.Call
import retrofit2.Response

class ExploreBarangFragment(val category1: String, val category2: String = "") : Fragment() {

    private var actionBar: ActionBar? = null
    private lateinit var call: Call<List<Barang>>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.explore_barang_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        actionBar = (activity!! as MainActivity).supportActionBar
        setupActionBar()

        explore_recycleview.layoutManager = GridLayoutManager(context!!, 2, GridLayoutManager.VERTICAL, false)
        explore_recycleview.addItemDecoration(DividerItemDecoration(context!!, DividerItemDecoration.VERTICAL))
        explore_recycleview.addItemDecoration(DividerItemDecoration(context!!, DividerItemDecoration.HORIZONTAL))
        callBarangEvent()
    }

    private fun setupActionBar() {
        actionBar?.show()
        actionBar?.setDisplayHomeAsUpEnabled(true)
        actionBar?.setHomeAsUpIndicator(R.drawable.ic_menu_black_24dp)
        actionBar?.title = ""

        activity!!.toolbar.setNavigationOnClickListener {
            activity!!.drawer_layout.openDrawer(GravityCompat.START)
        }

    }

    private fun callBarangEvent() {
        val httpClient = httpClient()
        val apiRequest = retrofitRequest<EventService>(httpClient)
        call = apiRequest.getBarangByKategori(arrayListOf(category1, category2))
        Log.d("explore", category1 + category2)
        val loadingFrag = LoadingFragment()
        fragmentManager!!.beginTransaction().add(R.id.frameLayout, loadingFrag).commit()
        call.enqueue(object : retrofit2.Callback<List<Barang>> {
            override fun onFailure(call: Call<List<Barang>>, t: Throwable) {
                Toast.makeText(context, "Failed fetch data (${t.message})", Toast.LENGTH_LONG).show()
            }

            override fun onResponse(call: Call<List<Barang>>, response: Response<List<Barang>>) {
                Log.d("explore", call.request().body.toString())
                explore_recycleview.adapter =
                    ExploreBarangAdapter(activity!! as MainActivity, context!!, response.body()!!)
                fragmentManager!!.beginTransaction().remove(loadingFrag).commit()
            }
        })
    }

    override fun onDestroy() {
        super.onDestroy()

        call.cancel()
    }

}
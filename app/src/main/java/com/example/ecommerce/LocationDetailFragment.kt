package com.example.ecommerce

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.ecommerce.DataClass.AlamatRo
import com.example.ecommerce.DataClass.LocalRoCity
import com.example.ecommerce.DataClass.LocalRoProvince
import kotlinx.android.synthetic.main.location_detail_fragment.*
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LocationDetailFragment(val idAddress: Int) : Fragment() {

    lateinit var listRoCity: List<LocalRoCity>
    lateinit var listRoProvince: List<LocalRoProvince>
    val loadingFrag = LoadingFragment()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.location_detail_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        callRoProvince()
        callRoCity()
        callRoLocationEvent()

    }

    private fun callRoCity() {
        val httpClient = OkHttpClient()
        val apiRequest = retrofitRequest<EventService>(httpClient)
        val call = apiRequest.getRoCity()
        call.enqueue(object : Callback<List<LocalRoCity>> {
            override fun onFailure(call: Call<List<LocalRoCity>>, t: Throwable) {
                Toast.makeText(context, "Failed Fetch 1 data", Toast.LENGTH_LONG).show()
                fragmentManager!!.beginTransaction().remove(loadingFrag).commit()
            }

            override fun onResponse(call: Call<List<LocalRoCity>>, response: Response<List<LocalRoCity>>) {
                listRoCity = response.body()!!
            }

        })
    }

    private fun callRoProvince() {
        val httpClient = OkHttpClient()
        val apiRequest = retrofitRequest<EventService>(httpClient)
        val call = apiRequest.getRoProvince()
        fragmentManager!!.beginTransaction().add(R.id.frameLayout, loadingFrag).commit()
        call.enqueue(object : Callback<List<LocalRoProvince>> {
            override fun onFailure(call: Call<List<LocalRoProvince>>, t: Throwable) {
                Toast.makeText(context, "Failed Fetch 0 data", Toast.LENGTH_LONG).show()
                fragmentManager!!.beginTransaction().remove(loadingFrag).commit()
            }

            override fun onResponse(call: Call<List<LocalRoProvince>>, response: Response<List<LocalRoProvince>>) {
                listRoProvince = response.body()!!
            }

        })
    }

    private fun callRoLocationEvent() {
        val httpClient = OkHttpClient()
        val apiRequest = retrofitRequest<EventService>(httpClient)
        val call = apiRequest.getUserRoAddress(idAddress)
        call.enqueue(object : Callback<AlamatRo> {
            override fun onFailure(call: Call<AlamatRo>, t: Throwable) {
                Log.d("loc", t.message)
                Toast.makeText(context, "Failed Fetch 2 data", Toast.LENGTH_LONG).show()
                fragmentManager!!.beginTransaction().remove(loadingFrag).commit()
            }

            override fun onResponse(call: Call<AlamatRo>, response: Response<AlamatRo>) {
                locationDetailName.text = response.body()!!.namaPenerima
                locationDetailAddress.text = response.body()!!.alamatLengkap
                locationDetailCity.text = response.body()!!.kota
                locationDetailProvince.text = response.body()!!.provinsi
                fragmentManager!!.beginTransaction().remove(loadingFrag).commit()
            }

        })
    }
}
package com.example.ecommerce

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.ecommerce.Cipher.decode
import com.example.ecommerce.DataClass.Barang
import kotlinx.android.synthetic.main.discoverstyle_item.view.*

class DiscoverStyleAdapter(val activity: MainActivity, val data: List<Barang>) :
    RecyclerView.Adapter<DiscoverStyleViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DiscoverStyleViewHolder {
        return DiscoverStyleViewHolder(parent)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: DiscoverStyleViewHolder, position: Int) {
        val barang = this.data[position]
        holder.itemView.discoveritemName.text = barang.name
        holder.itemView.setOnClickListener {
            activity.replaceFragmentToStack(DetailBarangFragment(barang), "")
        }
        Glide.with(holder.itemView).load(decode(barang.gambar)).into(holder.itemView.discoveritemImg)

    }

}
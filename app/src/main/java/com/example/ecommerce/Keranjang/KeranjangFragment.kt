package com.example.ecommerce.Keranjang

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.ActionBar
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.ecommerce.*
import com.example.ecommerce.DataClass.Keranjang
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.keranjang_layout.*
import kotlinx.android.synthetic.main.summary_price_view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class KeranjangFragment : Fragment() {

    lateinit var actionBar: ActionBar

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.keranjang_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        keranjangCheckout.setOnClickListener {
            (activity as MainActivity).replaceFragmentToStack(CheckoutFragment(), "");
        }

        actionBar = (activity!! as MainActivity).supportActionBar!!
        setupActionBar()

        keranjangRecycle.layoutManager = LinearLayoutManager(context!!, LinearLayoutManager.VERTICAL, false)
        callKeranjangEvent()

    }

    private fun setupActionBar() {
        actionBar.setDisplayHomeAsUpEnabled(true)
        actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)
        actionBar.title = "Keranjang"

        activity!!.toolbar.setNavigationOnClickListener {
            fragmentManager!!.popBackStack()
        }
    }

    private fun callKeranjangEvent() {
        val httpClient = httpClient()
        val apiRequest = retrofitRequest<EventService>(httpClient)
        val call = apiRequest.getItemCart(Config.loggedUser.id_user)
        val loadingFrag = LoadingFragment()
        fragmentManager!!.beginTransaction().add(R.id.frameLayout, loadingFrag).commit()
        call.enqueue(object : Callback<List<Keranjang>> {
            override fun onFailure(call: Call<List<Keranjang>>, t: Throwable) {
                Toast.makeText(context, "Failed fetch data (${t.message})", Toast.LENGTH_LONG).show()
            }

            override fun onResponse(call: Call<List<Keranjang>>, response: Response<List<Keranjang>>) {
                var totalItem = 0
                response.body()!!.iterator().forEach { totalItem += (it.hargaBarang * it.quantity) }
                summaryPriceTotalItem.text = totalItem.toString()
                summaryPriceTotal.text = (totalItem + 1000).toString()

                keranjangRecycle.adapter = KeranjangAdapter(activity!! as MainActivity, response.body()!!)
                fragmentManager!!.beginTransaction().remove(loadingFrag).commit()
            }

        })
    }

    override fun onDestroy() {
        super.onDestroy()

    }

}
package com.example.ecommerce.Keranjang

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.ecommerce.Cipher.decode
import com.example.ecommerce.DataClass.Keranjang
import com.example.ecommerce.MainActivity
import kotlinx.android.synthetic.main.keranjang_item.view.*

class KeranjangAdapter(val activity: MainActivity, val data: List<Keranjang>) :
    RecyclerView.Adapter<KeranjangViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): KeranjangViewHolder {
        return KeranjangViewHolder(parent)
    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: KeranjangViewHolder, position: Int) {
        val barang = data[position]
        holder.itemView.keranjangHarga.text = ("Rp. ${activity.currencyFormat(barang.hargaBarang)}")
        holder.itemView.keranjangNama.text = barang.nameBarang
        holder.itemView.keranjangQuantityNumber.setText(barang.quantity.toString())
        Glide.with(holder.itemView).load(decode(barang.gambarBarang)).into(holder.itemView.keranjangImg)

        var qty: Int = holder.itemView.keranjangQuantityNumber.text.toString().toInt()
        holder.itemView.keranjangPlusQty.setOnClickListener {
            qty += 1
            holder.itemView.keranjangQuantityNumber.setText(qty.toString())
        }

        holder.itemView.keranjangMinusQty.setOnClickListener {
            qty += if (qty <= 1) 0 else -1
            holder.itemView.keranjangQuantityNumber.setText(qty.toString())
        }

    }

}
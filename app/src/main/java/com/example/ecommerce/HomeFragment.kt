package com.example.ecommerce

import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.*
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import androidx.appcompat.app.ActionBar
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import com.example.ecommerce.ExploreBarang.ExploreBarangFragment
import com.example.ecommerce.Keranjang.KeranjangFragment
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.home_fragment.*
import kotlinx.android.synthetic.main.home_fragment.view.*

class HomeFragment : Fragment() {


    //Array, List, Other Collections
    private var imageViewList: Array<ImageView>? = null

    //Index
    private var currentImageView: ImageView? = null
    private var currentIndex = 0

    //Animation Variable
    private var animationFadeIn: Animation? = null
    private var animationFadeOut: Animation? = null

    //Other
    private var actionBar: ActionBar? = null
    private var handlerImageView = Handler()
    private var runnableImageView = object : Runnable {
        override fun run() {
            if (currentImageView != null) {
                animationFadeOut!!.reset()
                currentImageView!!.clearAnimation()
                currentImageView!!.startAnimation(animationFadeOut)
                currentImageView!!.visibility = View.INVISIBLE
            }
            currentImageView = imageViewList!![currentIndex]
            animationFadeIn!!.reset()
            currentImageView!!.clearAnimation()
            currentImageView!!.startAnimation(animationFadeIn)
            currentImageView?.visibility = View.VISIBLE
            currentIndex++
            if (currentIndex >= imageViewList!!.size) {
                currentIndex = 0
            }
            handlerImageView.postDelayed(this, 4000)
        }

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        setHasOptionsMenu(true)
        Log.d("fragment", "Fragment ${this.javaClass.name} Restored")
        return inflater.inflate(R.layout.home_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        actionBar = (activity as MainActivity).supportActionBar
        setupActionbar()

        imageViewList = arrayOf(view.homeImgView, view.homeImgView2, view.homeImgView3)

        initAnimation()
        initListener()

    }

    private fun initListener() {
        homeDiscoverStyle.setOnClickListener {
            (activity!! as MainActivity).replaceFragmentToStack(DiscoverStyleFragment(), "")
        }
        homeMensCollection.setOnClickListener {
            (activity!! as MainActivity).replaceFragmentToStack(ExploreBarangFragment("men"), "")
        }
        homeWomansCollection.setOnClickListener {
            (activity!! as MainActivity).replaceFragmentToStack(ExploreBarangFragment("woman"), "")
        }
    }

    private fun initAnimation() {
        animationFadeIn = AnimationUtils.loadAnimation(context, R.anim.fadein)
        animationFadeOut = AnimationUtils.loadAnimation(context, R.anim.fadeout)
    }

    private fun setupActionbar() {
        actionBar?.show()
        actionBar?.setDisplayHomeAsUpEnabled(true)
        actionBar?.setHomeAsUpIndicator(R.drawable.ic_menu_black_24dp)
        actionBar?.title = "Paradox"

        activity!!.toolbar.setNavigationOnClickListener {
            activity!!.drawer_layout.openDrawer(GravityCompat.START)
        }
    }

    override fun onResume() {
        super.onResume()
        ioThread { handlerImageView.post(runnableImageView) }
        Log.d("home", "onResume")
    }

    override fun onPause() {
        super.onPause()
        handlerImageView.removeCallbacks(runnableImageView)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        if (handlerImageView.hasCallbacks(runnableImageView)) {
            Log.d("home", "Remove ImageView Callback")
            handlerImageView.removeCallbacks(runnableImageView)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()
        inflater.inflate(R.menu.toolbar_menu_home, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_cart -> {
                (activity!! as MainActivity).replaceFragmentToStack(KeranjangFragment(), "")
                return true
            }

        }
        return true
    }

}
package com.example.ecommerce.base

import androidx.fragment.app.Fragment

abstract class ExtendedFragment : Fragment() {

    abstract fun onLoadingScreen()

    abstract fun startLoadingScreen()

    abstract fun destroyLoadingScreen()

    override fun onDestroy() {
        destroyLoadingScreen()
        super.onDestroy()
    }

}
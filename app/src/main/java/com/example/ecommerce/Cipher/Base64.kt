package com.example.ecommerce.Cipher

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Base64

fun toBitmap(data: String): Bitmap {
    val encodedString = "data:image/jpg;base64, ...."
    val pureBase64 = encodedString.substring(encodedString.indexOf(",") + 1)
    val decoded = Base64.decode(pureBase64, Base64.DEFAULT)
    return BitmapFactory.decodeByteArray(decoded, 0, decoded.size)
}

fun decode(data: String): ByteArray {
    return Base64.decode(data, Base64.DEFAULT)
}
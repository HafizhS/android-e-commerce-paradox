package com.example.ecommerce.Cipher

import okhttp3.internal.and

fun String.toHex(data: ByteArray): String {
    return "Hex"
}

fun toHex(data: ByteArray): String {
    val sb = StringBuilder()
    data.forEach {
        val v = it.toInt() and 0xFF
        var halfByte = (v.ushr(4)) and 0x0F
        var twoHalf = 0
        do {
            sb.append(if (halfByte in 0..9) '0' + halfByte else 'a' + (halfByte - 10))
            halfByte = it and 0x0F
        } while (twoHalf++ < 1)

    }
    return sb.toString()
}
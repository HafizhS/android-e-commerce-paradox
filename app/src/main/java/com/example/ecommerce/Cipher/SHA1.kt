package com.example.ecommerce.Cipher

import java.security.MessageDigest

fun toSha1(msg: String): String {
    val md = MessageDigest.getInstance("SHA-1")
    val textBytes = msg.toByteArray()
    md.update(textBytes, 0, textBytes.size)
    val sha1 = md.digest()
    return toHex(sha1)
}
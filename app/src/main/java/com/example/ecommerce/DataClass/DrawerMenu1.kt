package com.example.ecommerce.DataClass

import android.view.View
import androidx.fragment.app.Fragment

data class DrawerMenu1(
    val name: String,
    val listener: View.OnClickListener
)
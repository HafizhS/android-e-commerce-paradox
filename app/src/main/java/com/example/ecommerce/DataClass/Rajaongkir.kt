package com.example.ecommerce.DataClass

import com.google.gson.annotations.SerializedName

data class Rajaongkir<T>(
//    @SerializedName("query") val query : RajaongkirQuery,
    @SerializedName("status") val status: RajaongkirStatus,
    @SerializedName("results") val results: List<T>
)
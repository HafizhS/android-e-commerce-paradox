package com.example.ecommerce.DataClass

import com.google.gson.annotations.SerializedName

data class RajaongkirCity(
    @SerializedName("city_id") var id: Int,
    @SerializedName("province_id") var provinceId: Int,
    @SerializedName("province") var province: String,
    @SerializedName("type") var type: String,
    @SerializedName("city_name") var city: String,
    @SerializedName("postal_code") var postalCode: String
) {
    override fun toString(): String {
        return city
    }
}
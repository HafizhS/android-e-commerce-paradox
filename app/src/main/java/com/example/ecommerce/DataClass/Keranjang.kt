package com.example.ecommerce.DataClass

import com.google.gson.annotations.SerializedName

data class Keranjang(
    @SerializedName("id_barang") var idUser: Int,
    @SerializedName("id_keranjang") var idKeranjang: Int,
    @SerializedName("nama_barang") var nameBarang: String,
    @SerializedName("harga") var hargaBarang: Int,
    @SerializedName("gambar_barang") var gambarBarang: String,
    @SerializedName("quantity") val quantity: Int
)
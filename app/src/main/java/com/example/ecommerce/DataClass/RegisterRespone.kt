package com.example.ecommerce.DataClass

data class RegisterRespone(
    val message: String,
    val status: Int
)
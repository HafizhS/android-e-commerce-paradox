package com.example.ecommerce.DataClass

import com.google.gson.annotations.SerializedName

data class LocalRoProvince(
    @SerializedName("id_province") var idProvince: Int,
    @SerializedName("province") var province: String
)
package com.example.ecommerce.DataClass

import com.google.gson.annotations.SerializedName

data class DetailUser(
    @SerializedName("email") var email: String,
    @SerializedName("username") var username: String,
    @SerializedName("nama_lengkap") var fullname: String,
    @SerializedName("jenis_kelamin") var gender: String,
    @SerializedName("avatar") var avatar: String
)
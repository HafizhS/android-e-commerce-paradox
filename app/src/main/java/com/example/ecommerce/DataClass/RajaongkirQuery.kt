package com.example.ecommerce.DataClass

import com.google.gson.annotations.SerializedName


data class RajaongkirQuery(

    @SerializedName("key") val key: String
)
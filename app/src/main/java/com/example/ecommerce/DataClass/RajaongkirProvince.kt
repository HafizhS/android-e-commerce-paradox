package com.example.ecommerce.DataClass

import com.google.gson.annotations.SerializedName

data class RajaongkirProvince(
    @SerializedName("province_id") var id: Int,
    @SerializedName("province") var province: String
) {
    override fun toString(): String {
        return province
    }
}
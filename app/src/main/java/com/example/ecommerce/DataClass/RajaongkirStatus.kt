package com.example.ecommerce.DataClass

import com.google.gson.annotations.SerializedName

data class RajaongkirStatus(

    @SerializedName("code") val code: Int,
    @SerializedName("description") val description: String
)
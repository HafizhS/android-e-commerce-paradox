package com.example.ecommerce.DataClass

data class User(
    val id_user: Int,
    val email: String,
    val id_detail_user: Int,
    var id_level: Int
)
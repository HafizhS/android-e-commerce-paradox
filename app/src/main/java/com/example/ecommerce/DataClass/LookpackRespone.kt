package com.example.ecommerce.DataClass

import com.google.gson.annotations.SerializedName

data class LookpackRespone(
    @SerializedName("id_lookpack") val idBarang: Int,
    @SerializedName("nama_pack") val namaPack: String,
    @SerializedName("desc") val deskripsi: String,
    @SerializedName("model_pack") val gambar: String,
    @SerializedName("jenis_kelamin") val jenisKelamin: String
)
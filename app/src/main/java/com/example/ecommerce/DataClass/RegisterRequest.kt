package com.example.ecommerce.DataClass

import com.google.gson.annotations.SerializedName

data class RegisterRequest(
    val username: String,
    val email: String,
    @SerializedName("nama_lengkap") val fullName: String,
    val password: String
)
package com.example.ecommerce.DataClass

data class KeranjangRespone(
    val message: String,
    val status: Int,
    val isSuccess: Boolean
)
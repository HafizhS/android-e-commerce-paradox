package com.example.ecommerce.DataClass

data class Status(
    val code: Int = 0,
    val description: String = ""
)


data class Query(
    val courier: String = "",
    val origin: String = "",
    val destination: String = "",
    val weight: Int = 0
)


data class OriginDetails(
    val cityName: String = "",
    val province: String = "",
    val provinceId: String = "",
    val type: String = "",
    val postalCode: String = "",
    val cityId: String = ""
)


data class CostItem(
    val note: String = "",
    val etd: String = "",
    val value: Int = 0
)


data class ResultsItem(
    val costs: List<CostsItem>?,
    val code: String = "",
    val name: String = ""
)


data class CostsItem(
    val cost: List<CostItem>?,
    val service: String = "",
    val description: String = ""
)


data class DestinationDetails(
    val cityName: String = "",
    val province: String = "",
    val provinceId: String = "",
    val type: String = "",
    val postalCode: String = "",
    val cityId: String = ""
)



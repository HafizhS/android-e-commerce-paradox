package com.example.ecommerce.DataClass

data class Lookpack(
    val idBarang: Int,
    val namaPack: String,
    val deskripsi: String,
    val gambar: ByteArray,
    val jenisKelamin: String
)
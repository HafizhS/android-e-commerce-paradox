package com.example.ecommerce.DataClass

import com.google.gson.annotations.SerializedName

data class RajaongkirBase<T>(
    @SerializedName("rajaongkir") val rajaongkir: Rajaongkir<T>
)
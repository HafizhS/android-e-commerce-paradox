package com.example.ecommerce.DataClass

import com.google.gson.annotations.SerializedName

data class Barang(
    @SerializedName("id_barang") var id: Int,
    @SerializedName("nama_barang") var name: String,
    var deskripsi: String,
    var harga: Int,
    @SerializedName("gambar_barang") var gambar: String,
    @SerializedName("kategoris") val kategoris: List<Kategori>
)
package com.example.ecommerce.DataClass

import com.google.gson.annotations.SerializedName

data class LoginRespone(
    @SerializedName("message") var message: String,
    @SerializedName("id_user") var userId: Int,
    @SerializedName("id_detail_user") var detailUserId: Int,
    @SerializedName("id_level") var levelId: Int,
    @SerializedName("email") var email: String,
    @SerializedName("status") var status: Int,
    @SerializedName("rajaongkir_token") var roToken: String
)
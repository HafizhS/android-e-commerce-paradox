package com.example.ecommerce.DataClass

import com.google.gson.annotations.SerializedName

data class Alamat(
    @SerializedName("id_user_alamat") var idUserAlamat: Int,
    @SerializedName("id_user") var idUser: Int,
    @SerializedName("nama_pemilik") var namaPenerima: String,
    @SerializedName("alamat_lengkap") var alamatLengkap: String,
    @SerializedName("alamat_kota") var kota: String,
    @SerializedName("alamat_provinsi") var provinsi: String,
    @SerializedName("alamat_kodepos") var kodepos: String
)
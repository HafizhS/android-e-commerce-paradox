package com.example.ecommerce.DataClass

data class UserRequest(
    var email: String,
    var password: String
)
package com.example.ecommerce.DataClass

import com.google.gson.annotations.SerializedName

data class LocalRoCity(
    @SerializedName("id_city") var idCity: Int,
    @SerializedName("id_province") var idProvince: Int,
    @SerializedName("province") var province: String,
    @SerializedName("type") var type: String,
    @SerializedName("city_name") var cityName: String,
    @SerializedName("postal_code") var postalCode: String
)
package com.example.ecommerce.DataClass

import com.google.gson.annotations.SerializedName

data class Kategori(
    @SerializedName("id_kategori") val id_kategori: Int,
    @SerializedName("nama_kategori") val nama_kategori: String
//    ,@SerializedName("pivot") val pivot: HashMap<Int, Int>
)
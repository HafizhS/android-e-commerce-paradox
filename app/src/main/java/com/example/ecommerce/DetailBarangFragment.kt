package com.example.ecommerce

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.ActionBar
import androidx.fragment.app.Fragment
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.bumptech.glide.Glide
import com.example.ecommerce.Cipher.decode
import com.example.ecommerce.DataClass.Barang
import com.example.ecommerce.DataClass.KeranjangRespone
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.barang_detail.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DetailBarangFragment(var barang: Barang) : Fragment() {

    lateinit var actionBar: ActionBar

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.barang_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        actionBar = (activity!! as MainActivity).supportActionBar!!
        setupActionBar()

        val circularProgressDrawable = CircularProgressDrawable(context!!)
        circularProgressDrawable.strokeWidth = 5f
        circularProgressDrawable.centerRadius = 30f
        circularProgressDrawable.start()

        val formattedText = "Rp. ${(activity!! as MainActivity).currencyFormat(barang.harga)}"
        detailbarangTitle.text = barang.name
        detailbarangHarga.text = formattedText
        detailbarangDeskripsi.text = barang.deskripsi
        Glide.with(context!!).load(decode(barang.gambar)).placeholder(circularProgressDrawable).into(detailbarangImg)

        detailbarangAddtoCart.setOnClickListener {
            insertEventKerajang()
        }

    }

    private fun setupActionBar() {
        actionBar.setDisplayHomeAsUpEnabled(true)
        actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)
        actionBar.title = ""

        activity!!.toolbar.setNavigationOnClickListener {
            fragmentManager!!.popBackStack()
        }
    }

    private fun insertEventKerajang() {
        val httpClient = httpClient()
        val apiRequest = retrofitRequest<EventService>(httpClient)
        val call = apiRequest.insertKeranjang(Config.loggedUser.id_user, barang.id, 1)
        val loading = LoadingFragment()
        fragmentManager!!.beginTransaction().add(R.id.frameLayout, loading).commit()
        call.enqueue(object : Callback<KeranjangRespone> {
            override fun onFailure(call: Call<KeranjangRespone>, t: Throwable) {
                Toast.makeText(context, "Failed fetch data (${t.message})", Toast.LENGTH_LONG).show()
                fragmentManager!!.beginTransaction().remove(loading).commit()
            }

            override fun onResponse(call: Call<KeranjangRespone>, response: Response<KeranjangRespone>) {
                Toast.makeText(context!!, response.body()!!.message, Toast.LENGTH_SHORT).show()
                fragmentManager!!.beginTransaction().remove(loading).commit()
                fragmentManager!!.popBackStack()
            }

        })
    }

}
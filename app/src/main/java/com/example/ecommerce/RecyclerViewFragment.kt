package com.example.ecommerce

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.ActionBar
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.recyclerview_fragment.*

class RecyclerViewFragment<VH : RecyclerView.ViewHolder>(
    private var title: String,
    private var layoutManager: RecyclerView.LayoutManager,
    private var adapter: RecyclerView.Adapter<VH>
) : Fragment() {

    private var actionBar: ActionBar? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.recyclerview_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        actionBar = (activity as MainActivity).supportActionBar
        setupActionbar()

        recyclerview_recylcer.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        recyclerview_recylcer.adapter = adapter
    }

    private fun setupActionbar() {
        actionBar?.show()
        actionBar?.setDisplayHomeAsUpEnabled(true)
        actionBar?.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)
        actionBar?.title = title

        activity!!.toolbar.setNavigationOnClickListener {
            fragmentManager!!.popBackStack()
        }
    }
}
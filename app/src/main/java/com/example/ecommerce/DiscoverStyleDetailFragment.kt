package com.example.ecommerce

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.ecommerce.DataClass.Barang
import com.example.ecommerce.DataClass.Lookpack
import kotlinx.android.synthetic.main.discoverdetail_fragment.*
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DiscoverStyleDetailFragment(val lookpack: Lookpack) : Fragment() {

    lateinit var call: Call<List<Barang>>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.discoverdetail_fragment, container, false)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        dicoverdetailList.layoutManager = LinearLayoutManager(context!!, LinearLayoutManager.VERTICAL, false)
        callLookpackDetail()

    }

    private fun callLookpackDetail() {
        val httpClient = OkHttpClient()
        val apiRequest = retrofitRequest<EventService>(httpClient)
        call = apiRequest.getlookpackDetails(lookpack.idBarang)
        call.enqueue(object : Callback<List<Barang>> {
            override fun onFailure(call: Call<List<Barang>>, t: Throwable) {
                if (!call.isCanceled) return
                Toast.makeText(context, "Failed fetch data (${t.message})", Toast.LENGTH_LONG).show()
            }

            override fun onResponse(call: Call<List<Barang>>, response: Response<List<Barang>>) {
                if (call.isCanceled) return
                dicoverdetailList.adapter = DiscoverStyleAdapter((activity!! as MainActivity), response.body()!!)
            }
        })
    }

    override fun onDestroy() {
        call.cancel()
        super.onDestroy()
    }
}
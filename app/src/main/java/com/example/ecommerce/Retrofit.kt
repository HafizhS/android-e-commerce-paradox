package com.example.ecommerce

import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.internal.toHostHeader
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

inline fun <reified T> retrofitRequest(okHttpClient: OkHttpClient): T {
    val gson = GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create()
    val retrofit = Retrofit.Builder()
        .baseUrl("${Config.SERVER_SCHEME}://${Config.SERVER_URL}:${Config.SERVER_PORT}/${Config.SERVER_SUB_URL}/")
        .addConverterFactory(GsonConverterFactory.create(gson))
        .build()
    return retrofit.create(T::class.java)

}


inline fun <reified T> rajaongkirRequest(okHttpClient: OkHttpClient): T {
    val gson = GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create()
    val retrofit = Retrofit.Builder()
        .baseUrl("https://api.rajaongkir.com/starter/")
        .addConverterFactory(GsonConverterFactory.create(gson))
        .client(okHttpClient)
        .build()
    return retrofit.create(T::class.java)
}
//abstract fun <T> callApiEvent(context: Context, event: EventService) {
//    val httpClient = httpClient()
//    val apiRequest = retrofitRequest<EventService>(httpClient)
//    val call = apiRequest.mey
//    call.enqueue(object : retrofit2.Callback<List<Barang>> {
//        override fun onFailure(call: Call<List<Barang>>, t: Throwable) {
//        }
//
//        override fun onResponse(call: Call<List<Barang>>, response: Response<List<Barang>>) {
//        }
//    }
//}
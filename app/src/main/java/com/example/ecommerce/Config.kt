package com.example.ecommerce

import android.graphics.Bitmap
import com.example.ecommerce.DataClass.User

class Config {
    companion object {
        const val SERVER_SCHEME = "http"
        //default for emulator 10.0.2.2
        //from hotspot 192.168.137.1
        //from phone hotspot 192.168.43.149
        //192.168.22.189
        const val SERVER_URL = "192.168.22.189"
        const val SERVER_PORT = "8000"
        const val SERVER_SUB_URL = "api"

        var API_TOKEN = null
        lateinit var RAJAONGKIR_API_TOKEN: String
        var isLoggedIn = false
        lateinit var loggedUser: User

        var bitmap: Bitmap? = null
        fun getROToken(): String = RAJAONGKIR_API_TOKEN
    }
}

package com.example.ecommerce

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import com.example.ecommerce.DataClass.Alamat
import com.example.ecommerce.DataClass.AlamatRo
import com.example.ecommerce.DataClass.RajaongkirCost.Json4Kotlin_Base
import kotlinx.android.synthetic.main.checkout_fragment.*
import kotlinx.android.synthetic.main.location_item.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CheckoutFragment : Fragment() {

    lateinit var alamatRo: AlamatRo
    val loadingFrag = LoadingFragment()
    var selectedCourier: String? = null
    val courier = listOf("jne", "pos", "tiki")

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.checkout_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        callRajaOngkirAlamat()

        checkoutCourier.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                val data = (p0!!.selectedItem as String)
                selectedCourier = data
                callRajaOngkirCost()
            }
        }


    }

    private fun callRajaOngkirAlamat() {
        val httpClient = httpClient()
        val apirequest = retrofitRequest<EventService>(httpClient)
        val call = apirequest.getUserAddress(Config.loggedUser.id_user)
        fragmentManager!!.beginTransaction().add(R.id.frameLayout, loadingFrag).commit()
        call.enqueue(object : Callback<List<Alamat>> {
            override fun onFailure(call: Call<List<Alamat>>, t: Throwable) {
                fragmentManager!!.beginTransaction().remove(loadingFrag).commit()
            }

            override fun onResponse(call: Call<List<Alamat>>, response: Response<List<Alamat>>) {
                val apirequest2 = retrofitRequest<EventService>(httpClient)
                val call2 = apirequest2.getUserRoAddress(response.body()!![0].idUserAlamat)
                call2.enqueue(object : Callback<AlamatRo> {
                    override fun onFailure(call: Call<AlamatRo>, t: Throwable) {
                        fragmentManager!!.beginTransaction().remove(loadingFrag).commit()
                    }

                    override fun onResponse(call: Call<AlamatRo>, response: Response<AlamatRo>) {
                        alamatRo = response.body()!!
                        locationCity.text = "City: ${alamatRo.kota}"
                        locationProvince.text = "Province: ${alamatRo.provinsi}"
                        locationAddress.text = "Province: ${alamatRo.alamatLengkap}"
                        checkoutCourier.adapter = CustomArrayAdapter(context!!, courier)
                        fragmentManager!!.beginTransaction().remove(loadingFrag).commit()
                    }
                })
            }

        })
    }

    private fun callRajaOngkirCost() {
        val httpClient = httpClientRajaOngkir()
        val apirequest = rajaongkirRequest<EventServiceRajaOngkir>(httpClient)
        val call = apirequest.getCost(444, alamatRo.roCity, 1000, selectedCourier!!)
        fragmentManager!!.beginTransaction().add(R.id.frameLayout, loadingFrag).commit()
        call.enqueue(object : Callback<Json4Kotlin_Base> {
            override fun onFailure(call: Call<Json4Kotlin_Base>, t: Throwable) {
                fragmentManager!!.beginTransaction().remove(loadingFrag).commit()
            }

            override fun onResponse(call: Call<Json4Kotlin_Base>, response: Response<Json4Kotlin_Base>) {
                checkoutService.adapter = CustomArrayAdapter(context!!, response.body()!!.rajaongkir.results[0].costs)
                fragmentManager!!.beginTransaction().remove(loadingFrag).commit()

            }
        })
    }

    class CustomArrayAdapter<T>(context: Context, var data: List<T>) :
        ArrayAdapter<T>(context, android.R.layout.simple_list_item_1, data) {

        override fun getItem(position: Int): T? {
            return data[position]
        }

    }
}
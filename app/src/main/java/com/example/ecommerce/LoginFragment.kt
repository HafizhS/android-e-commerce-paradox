package com.example.ecommerce

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.example.ecommerce.Cipher.toSha1
import com.example.ecommerce.DataClass.LoginRespone
import com.example.ecommerce.DataClass.User
import com.example.ecommerce.DataClass.UserRequest
import kotlinx.android.synthetic.main.login_fragment.*
import retrofit2.Call
import retrofit2.Response

class LoginFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.login_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity!! as MainActivity).supportActionBar!!.hide()

        loginBtnLogin.setOnClickListener {
            callApiEvent()
        }

        loginRegister.setOnClickListener {
            fragmentManager!!.beginTransaction()
                .setCustomAnimations(
                    R.anim.anim_slide_in_right,
                    R.anim.anim_slide_out_left,
                    R.anim.anim_slide_in_left,
                    R.anim.anim_slide_out_right
                )
                .replace(R.id.frameLayout, RegisterFragment())
                .addToBackStack(null)
                .commit()
        }
    }

    private fun callApiEvent() = ioThread {
        val httpClient = httpClient()
        val apiRequest = retrofitRequest<EventService>(httpClient)
        val password = toSha1(loginEdtPassword.text.toString())
        val loadingFrag = LoadingFragment()
        val call = apiRequest.loginRequest(UserRequest(loginEdtEmail.text.toString(), password))
        fragmentManager!!.beginTransaction().add(R.id.frameLayout, loadingFrag).commit()
        call.enqueue(object : retrofit2.Callback<LoginRespone> {
            override fun onFailure(call: Call<LoginRespone>, t: Throwable) {
                Toast.makeText(context, "Failed fetch data (${t.message})", Toast.LENGTH_LONG).show()
                fragmentManager!!.beginTransaction().remove(loadingFrag).commit()
            }

            override fun onResponse(call: Call<LoginRespone>, response: Response<LoginRespone>) {
                if (response.isSuccessful) {
                    Toast.makeText(context, response.body()?.message, Toast.LENGTH_LONG).show()
                    if (response.body()!!.status == 1) {
                        val respone = response.body()!!
                        Log.d("login", response.body()!!.detailUserId.toString())
                        Config.loggedUser = User(
                            respone.userId,
                            respone.email,
                            respone.detailUserId,
                            respone.levelId
                        )
                        Config.isLoggedIn = true
                        Config.RAJAONGKIR_API_TOKEN = respone.roToken
                        activity!!.supportFragmentManager.beginTransaction()
                            .replace(R.id.frameLayout, HomeFragment(), MainActivity.HOME_TAG)
                            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                            .commit()
                    }
                } else {
                    Log.d("login", call.request().url.toString())
                    Toast.makeText(context, response.body()?.message, Toast.LENGTH_LONG).show()
                }
                fragmentManager!!.beginTransaction().remove(loadingFrag).commit()
            }
        })
    }
}
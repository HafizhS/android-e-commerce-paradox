package com.example.ecommerce

import android.os.Bundle
import android.os.Handler
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.example.ecommerce.NavigationView.NavigationViewFragment
import kotlinx.android.synthetic.main.activity_main.*
import java.text.DecimalFormat

class MainActivity : AppCompatActivity() {

    var fragmentManager: FragmentManager? = null
    var doubleBackPressed = false

    companion object {
        const val HOME_TAG = "home"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, GravityCompat.END)

        fragmentManager = supportFragmentManager
        setSupportActionBar(toolbar)

        fragmentManager!!.beginTransaction().replace(R.id.navigation_frame, NavigationViewFragment()).commit()

        if (!Config.isLoggedIn) {
            replaceFragment(LoginFragment(), "")
            return
        }
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START) || drawer_layout.isDrawerOpen(GravityCompat.END)) {
            if (fragmentManager!!.findFragmentByTag("nav_fragment") != null) {
                fragmentManager!!.beginTransaction().remove(fragmentManager!!.findFragmentByTag("nav_fragment")!!)
                    .commit()
            } else {
                drawer_layout.closeDrawer(GravityCompat.START)
                drawer_layout.closeDrawer(GravityCompat.END)
            }
            return
        }

        if (fragmentManager!!.backStackEntryCount == 0) {
            if (doubleBackPressed) {
                super.onBackPressed()
                return
            }
            doubleBackPressed = true
            Toast.makeText(this, "Press back again to close App", Toast.LENGTH_SHORT).show()
            Handler().postDelayed({ doubleBackPressed = false }, 1500)
            return
        }

        super.onBackPressed()

    }

    fun replaceFragment(fragment: Fragment, tag: String, force: Boolean = false) {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) drawer_layout
            .closeDrawer(GravityCompat.START)

        val oldFragment = fragmentManager!!.findFragmentByTag(tag)
        if (oldFragment != null && !force) {
            fragmentManager!!.beginTransaction()
                .remove(oldFragment)
                .commit()

        }

        fragmentManager!!.beginTransaction()
            .replace(R.id.frameLayout, fragment, tag)
            .commit()

    }

    fun replaceFragmentToStack(fragment: Fragment, tag: String) {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) drawer_layout
            .closeDrawer(GravityCompat.START)

        fragmentManager!!
            .beginTransaction()
            .setCustomAnimations(R.anim.rtl_enter, R.anim.lrt_out, R.anim.rtl_enter, R.anim.lrt_out)
            .replace(R.id.frameLayout, fragment)
            .addToBackStack(tag)
            .commit()
    }

    fun currencyFormat(value: Any): String {
        val formatter = DecimalFormat("#,###")
        return formatter.format(value).replace(',', '.')
    }

}

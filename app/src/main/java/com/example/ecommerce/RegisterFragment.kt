package com.example.ecommerce

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.ecommerce.Cipher.toSha1
import com.example.ecommerce.DataClass.RegisterRequest
import com.example.ecommerce.DataClass.RegisterRespone
import kotlinx.android.synthetic.main.register_fragment.*
import retrofit2.Call
import retrofit2.Response

class RegisterFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.register_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        registerSubmit.setOnClickListener {
            callRegisterEvent()

        }
    }


    private fun callRegisterEvent() {
        val httpClient = httpClient()
        val apiRequest = retrofitRequest<EventService>(httpClient)
        val password = toSha1(registerPassword.text.toString())
        val loadingFrag = LoadingFragment()
        val call = apiRequest.registerRequest(
            RegisterRequest(
                username = registerUsername.text.toString(),
                email = registerEmail.text.toString(),
                fullName = registerFullName.text.toString(),
                password = password
            )
        )
        fragmentManager!!.beginTransaction().add(R.id.frameLayout, loadingFrag).commit()
        call.enqueue(object : retrofit2.Callback<RegisterRespone> {
            override fun onFailure(call: Call<RegisterRespone>, t: Throwable) {
                Toast.makeText(context, "Failed fetch data (${t.message})", Toast.LENGTH_LONG).show()
            }

            override fun onResponse(call: Call<RegisterRespone>, response: Response<RegisterRespone>) {
                if (response.isSuccessful) {
                    Toast.makeText(context, response.body()?.message, Toast.LENGTH_LONG).show()
                    if (response.body()!!.status == 1) {
                        Toast.makeText(context!!, response.body()!!.message, Toast.LENGTH_SHORT).show()
//                        activity!!.supportFragmentManager.beginTransaction()
//                            .replace(R.id.frameLayout, HomeFragment())
//                            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
//                            .commit()
                    }
                } else {
                    Log.d("register", call.request().url.toString())
                    Toast.makeText(context, response.body()?.message, Toast.LENGTH_LONG).show()
                }
                fragmentManager!!.beginTransaction().remove(loadingFrag).commit()
            }
        })
    }
}
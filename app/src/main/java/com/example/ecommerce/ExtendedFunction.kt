package com.example.ecommerce

import android.util.Base64
import okhttp3.internal.and
import java.security.MessageDigest

fun String.decode(): ByteArray {
    return Base64.decode(this, Base64.DEFAULT)
}

fun ByteArray.toHex(): String {
    val sb = StringBuilder()
    this.forEach {
        val v = it.toInt() and 0xFF
        var halfByte = (v.ushr(4)) and 0x0F
        var twoHalf = 0
        do {
            sb.append(if (halfByte in 0..9) '0' + halfByte else 'a' + (halfByte - 10))
            halfByte = it and 0x0F
        } while (twoHalf++ < 1)

    }
    return sb.toString()
}

fun String.toSha1(): String {
    val md = MessageDigest.getInstance("SHA-1")
    val textBytes = this.toByteArray()
    md.update(textBytes, 0, textBytes.size)
    val sha1 = md.digest()
    return sha1.toHex()
}
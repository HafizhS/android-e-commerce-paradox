package com.example.ecommerce

import com.example.ecommerce.DataClass.*
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.http.*
import retrofit2.http.Query

interface EventService {

    @GET("barang")
    fun getBarang(): Call<List<Barang>>

    @GET("test1")
    fun getBarangByKategori(@Query("category[]") category: ArrayList<String>): Call<List<Barang>>

    @POST("barang")
    fun insertBarang(): Void

    @GET("barang/{id}")
    fun updateBarang(@Path("id") id: Int): Void

    @POST("barang/{id}")
    fun deleteBarang(@Path("id") id: Int): Void

    @POST("login")
    fun loginRequest(@Body userRequest: UserRequest): Call<LoginRespone>

    @POST("register")
    fun registerRequest(@Body registerRequest: RegisterRequest): Call<RegisterRespone>

    @GET("lookpack")
    fun getLookpack(): Call<List<LookpackRespone>>

    @GET("kategori")
    fun getKategori(): Call<List<Kategori>>

    @POST("keranjang/add")
    fun insertKeranjang(@Query("id_user") idUser: Int, @Query("id_barang") idBarang: Int, @Query("quantity") quantity: Int): Call<KeranjangRespone>

    @GET("keranjang")
    fun getItemCart(@Query("id_user") id_user: Int): Call<List<Keranjang>>

    @GET("lookpack/detail")
    fun getlookpackDetails(@Query("id_lookpack") idLookpack: Int): Call<List<Barang>>

    @GET("user")
    fun getUser(): Call<User>

    @GET("user/detail")
    fun getUserDetail(@Query("id_detail_user") idDetailUser: Int): Call<DetailUser>

    @GET("user/alamat")
    fun getUserAddress(@Query("id_user") idUser: Int): Call<List<Alamat>>

    @GET("user/alamat/ro")
    fun getUserRoAddress(@Query("id_user_alamat") idUserAlamat: Int): Call<AlamatRo>

    @GET("alamat/city")
    fun getRoCity(): Call<List<LocalRoCity>>

    @GET("alamat/province")
    fun getRoProvince(): Call<List<LocalRoProvince>>

    @Multipart
    @POST("user/update")
    fun updateImage(@Part photo: MultipartBody.Part)

}
package com.example.ecommerce

import com.example.ecommerce.DataClass.RajaongkirBase
import com.example.ecommerce.DataClass.RajaongkirCity
import com.example.ecommerce.DataClass.RajaongkirCost.Json4Kotlin_Base
import com.example.ecommerce.DataClass.RajaongkirProvince
import retrofit2.Call
import retrofit2.http.*

interface EventServiceRajaOngkir {

    @GET("province")
    fun getProvince(): Call<RajaongkirBase<RajaongkirProvince>>

    @GET("city")
    fun getCityByProvince(@Query("province") provinceId: Int): Call<RajaongkirBase<RajaongkirCity>>

    @GET("city")
    fun getCity(): Call<RajaongkirBase<RajaongkirCity>>

    @FormUrlEncoded
    @POST("cost")
    fun getCost(
        @Field("origin") origin: Int,
        @Field("destination") destination: Int,
        @Field("weight") weight: Int,
        @Field("courier") courier: String
    ): Call<Json4Kotlin_Base>
}
package com.example.ecommerce

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.ActionBar
import androidx.fragment.app.Fragment
import com.example.ecommerce.DataClass.DetailUser
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.profile_fragment.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ProfileFragment : Fragment() {

    private var actionBar: ActionBar? = null
    private lateinit var call: Call<DetailUser>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.profile_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        profileLocationAddress.setOnClickListener {
            (activity as MainActivity).replaceFragmentToStack(LocationFragment(), "")
        }

        actionBar = (activity as MainActivity).supportActionBar
        setupActionbar()

        callDetailUserEvent()
    }

    private fun setupActionbar() {
        actionBar?.show()
        actionBar?.setDisplayHomeAsUpEnabled(true)
        actionBar?.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)
        actionBar?.title = "Profile"

        activity!!.toolbar.setNavigationOnClickListener {
            fragmentManager!!.popBackStack()
        }
    }

    private fun callDetailUserEvent() {
        val httpClient = httpClient()
        val apiRequest = retrofitRequest<EventService>(httpClient)
        val loadingFrag = LoadingFragment()
        call = apiRequest.getUserDetail(Config.loggedUser.id_detail_user)
        fragmentManager!!.beginTransaction().add(R.id.frameLayout, loadingFrag).commit()
        call.enqueue(object : Callback<DetailUser> {
            override fun onFailure(call: Call<DetailUser>, t: Throwable) {
                fragmentManager!!.beginTransaction().remove(loadingFrag).commit()
            }

            override fun onResponse(call: Call<DetailUser>, response: Response<DetailUser>) {
                val data = response.body()!!
                Log.d("profile", response.message())
                Log.d("profile", response.toString())
                Log.d("profile", response.body()!!.fullname)
                profileEmail.text = data.email
                profileFullname.text = data.fullname
                profileUsername.text = data.username
                profileGender.text = data.gender
                fragmentManager!!.beginTransaction().remove(loadingFrag).commit()
            }

        })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        call.cancel()
    }
}
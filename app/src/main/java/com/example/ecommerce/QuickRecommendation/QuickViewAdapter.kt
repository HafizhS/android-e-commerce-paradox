package com.example.ecommerce.QuickRecommendation

import android.content.Context
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.ecommerce.Cipher.decode
import com.example.ecommerce.DataClass.Barang
import kotlinx.android.synthetic.main.home_quick_recommendation_item.view.*

class QuickViewAdapter(val data: List<Barang>, val context: Context) : RecyclerView.Adapter<QuickViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): QuickViewHolder =
        QuickViewHolder(parent)

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: QuickViewHolder, position: Int) {
        val barang = data[position]
        holder.itemView.qriHarga.text = barang.harga.toString()
        holder.itemView.qriNama.text = barang.name
        Glide.with(context).load(decode(barang.gambar)).into(holder.itemView.qriImage)
    }
}
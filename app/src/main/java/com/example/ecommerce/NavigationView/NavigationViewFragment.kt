package com.example.ecommerce.NavigationView

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.ecommerce.AboutFragment
import com.example.ecommerce.DataClass.DrawerMenu2
import com.example.ecommerce.MainActivity
import com.example.ecommerce.ProfileFragment
import com.example.ecommerce.R
import kotlinx.android.synthetic.main.nav_header_main.*
import kotlinx.android.synthetic.main.navmenu1_fragment.*
import kotlinx.android.synthetic.main.navmenu1_menu.*

class NavigationViewFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.navmenu1_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initListener()
    }

    private fun initListener() {

        navUser.setOnClickListener {
            (activity!! as MainActivity).replaceFragmentToStack(ProfileFragment(), "")
        }

        menu1Mens.setOnClickListener {
            nextNav("men")
        }

        menu1Womans.setOnClickListener {
            nextNav("woman")
        }

        menu1Kids.setOnClickListener {
            nextNav("kids")
        }

        navHome.setOnClickListener {
            val homeFragment = fragmentManager!!.findFragmentByTag(MainActivity.HOME_TAG)
            (activity!! as MainActivity).replaceFragment(homeFragment!!, MainActivity.HOME_TAG)
        }

        navAbout.setOnClickListener {
            (activity!! as MainActivity).replaceFragmentToStack(AboutFragment(), "")
        }
    }

    private fun replaceFragment(fragment: Fragment) {
        fragmentManager!!.beginTransaction()
            .setCustomAnimations(R.anim.lrt_enter, 0)
            .add(R.id.navigation, fragment, "nav_fragment").commit()
    }

    private fun nextNav(category: String) {
        replaceFragment(NavigationViewFragment2(DrawerMenu2(category)))
    }
}
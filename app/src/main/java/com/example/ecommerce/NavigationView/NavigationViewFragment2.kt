package com.example.ecommerce.NavigationView

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.ecommerce.*
import com.example.ecommerce.DataClass.DrawerMenu2
import com.example.ecommerce.DataClass.Kategori
import com.example.ecommerce.DrawerNavView.NavigationMenu2Adapter
import kotlinx.android.synthetic.main.navmenu2_fragment.*
import retrofit2.Call
import retrofit2.Response
import java.util.*

class NavigationViewFragment2(var drawerMenu2: DrawerMenu2) : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.navmenu2_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        menu2_toolbar.title = drawerMenu2.category1.toUpperCase(Locale.getDefault())

        menu2_list.layoutManager = LinearLayoutManager(context!!, LinearLayoutManager.VERTICAL, false)
        callKategoriEvent()

        menu2_toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp)
        menu2_toolbar.setNavigationOnClickListener {
            fragmentManager!!.beginTransaction().remove(this).commit()
        }
    }

    private fun callKategoriEvent() {
        val httpClient = httpClient()
        val apiRequest = retrofitRequest<EventService>(httpClient)
        val call = apiRequest.getKategori()
        val loadingFragment = LoadingFragment()
        fragmentManager!!.beginTransaction().add(R.id.navigation, loadingFragment).commit()
        call.enqueue(object : retrofit2.Callback<List<Kategori>> {
            override fun onFailure(call: Call<List<Kategori>>, t: Throwable) {
                Log.d("home", "onFailure: ${t.message}")
            }

            override fun onResponse(call: Call<List<Kategori>>, response: Response<List<Kategori>>) {
                val newList =
                    response.body()!!.filterNot { kategori -> kategori.nama_kategori == "men" || kategori.nama_kategori == "woman" }
                menu2_list.adapter = NavigationMenu2Adapter(newList, drawerMenu2)
                fragmentManager!!.beginTransaction().remove(loadingFragment).commit()
            }
        })
    }
}
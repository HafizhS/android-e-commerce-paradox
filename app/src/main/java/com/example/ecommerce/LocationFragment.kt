package com.example.ecommerce

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.ecommerce.DataClass.Alamat
import kotlinx.android.synthetic.main.location_fragment.*
import kotlinx.android.synthetic.main.location_item.view.*
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LocationFragment : Fragment() {


    lateinit var call: Call<List<Alamat>>
    lateinit var locationList: List<Alamat>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.location_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        callLocationEvent()

        locationRecyclerView.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
    }

    private fun callLocationEvent() {
        val httpClient = OkHttpClient()
        val apiRequest = retrofitRequest<EventService>(httpClient)
        val loadingFrag = LoadingFragment()
        call = apiRequest.getUserAddress(Config.loggedUser.id_user)
        fragmentManager!!.beginTransaction().add(R.id.frameLayout, loadingFrag).commit()
        call.enqueue(object : Callback<List<Alamat>> {
            override fun onFailure(call: Call<List<Alamat>>, t: Throwable) {
                Toast.makeText(context, "Failed Fetch data", Toast.LENGTH_LONG).show()
                fragmentManager!!.beginTransaction().remove(loadingFrag).commit()
            }

            override fun onResponse(call: Call<List<Alamat>>, response: Response<List<Alamat>>) {
                locationList = response.body()!!
                Log.d("location", response.body()!![0].alamatLengkap)
                locationRecyclerView.adapter = LocationAdapter(activity as MainActivity, context!!, locationList)
                fragmentManager!!.beginTransaction().remove(loadingFrag).commit()
            }

        })
    }

    override fun onDestroy() {
        super.onDestroy()

        call.cancel()
    }

    class LocationAdapter(val activity: MainActivity, val context: Context, var data: List<Alamat>) :
        RecyclerView.Adapter<LocationAdapter.Viewholder>() {

        class Viewholder(val parent: ViewGroup) :
            RecyclerView.ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.location_item, parent, false))

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Viewholder {
            return Viewholder(parent)
        }

        override fun getItemCount(): Int {
            return data.size
        }

        override fun onBindViewHolder(holder: Viewholder, position: Int) {
            val location = data[position]
            holder.itemView.setOnClickListener {
                activity.replaceFragmentToStack(LocationDetailFragment(location.idUserAlamat), "")
            }
            holder.itemView.locationProvince.text = "Province: ${location.provinsi}"
            holder.itemView.locationCity.text = "City: ${location.kota}"
            holder.itemView.locationAddress.text = location.alamatLengkap
        }
    }
}
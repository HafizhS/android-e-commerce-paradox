package com.example.ecommerce

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.os.Debug
import android.util.Log
import android.view.*
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.Toast
import androidx.appcompat.app.ActionBar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.transition.DrawableCrossFadeTransition
import com.bumptech.glide.request.transition.Transition
import com.bumptech.glide.request.transition.TransitionFactory
import com.example.ecommerce.Cipher.decode
import com.example.ecommerce.DataClass.Lookpack
import com.example.ecommerce.DataClass.LookpackRespone
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.discoverstyle_fragment.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DiscoverStyleFragment : Fragment() {

    //Lookpack things
    private var menLookpack: ArrayList<Lookpack>? = null
    private var womanLookpack: ArrayList<Lookpack>? = null
    private var selectedLookpack: ArrayList<Lookpack>? = null
    private var lookpack: Lookpack? = null
    private val MODEL_MEN = "men"
    private val MODEL_WOMAN = "woman"
    private var currentMenIndex: Int = 0
    private var currentWomanIndex: Int = 0
    private var selectedStyle = "woman"

    private var animationRtl: Animation? = null
    private var animationLtr: Animation? = null

    var actionBar: ActionBar? = null

    var call: Call<List<LookpackRespone>>? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.discoverstyle_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        actionBar = (activity as MainActivity).supportActionBar
        setupToolbar()

        initAnimation()
        initListener()

        menLookpack = arrayListOf()
        womanLookpack = arrayListOf()

        callLookEvent()
        activity!!.drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNDEFINED, GravityCompat.END)
    }

    private fun setupToolbar() {
        actionBar?.setDisplayHomeAsUpEnabled(true)
        actionBar?.setHomeAsUpIndicator(R.drawable.ic_menu_black_24dp)
        actionBar?.title = ""
        activity!!.toolbar.setNavigationOnClickListener {
            activity!!.drawer_layout.openDrawer(GravityCompat.START)
        }
    }

    private fun initAnimation() {
        animationRtl = AnimationUtils.loadAnimation(context!!, R.anim.rtl_out)
        animationLtr = AnimationUtils.loadAnimation(context!!, R.anim.lrt_enter)
    }

    private fun initListener() {
        homeLeftArrow.setOnClickListener {
            if (selectedStyle == MODEL_WOMAN) {
                currentWomanIndex--
                if (currentWomanIndex == -1) {
                    currentWomanIndex = womanLookpack!!.size - 1
                }
                changeLookpack(selectedLookpack?.get(currentWomanIndex)!!)
            } else if (selectedStyle == MODEL_MEN) {
                currentMenIndex--
                if (currentMenIndex == -1) {
                    currentMenIndex = menLookpack!!.size - 1
                }
                changeLookpack(selectedLookpack?.get(currentMenIndex)!!)
            }

        }

        homeRightArrow.setOnClickListener {
            if (selectedStyle == MODEL_WOMAN) {
                currentWomanIndex++
                if (currentWomanIndex > womanLookpack!!.size - 1) {
                    currentWomanIndex = 0
                }
                changeLookpack(selectedLookpack?.get(currentWomanIndex)!!)
            } else if (selectedStyle == MODEL_MEN) {
                currentMenIndex++
                if (currentMenIndex > menLookpack!!.size - 1) {
                    currentMenIndex = 0
                }
                changeLookpack(selectedLookpack?.get(currentMenIndex)!!)
            }
        }

        homeTvWoman.setOnClickListener {
            homeTvWoman.setTextColor(resources.getColor(R.color.colorAccent))
            homeTvMen.setTextColor(resources.getColor(R.color.colorPrimaryDark))
            selectedLookpack = womanLookpack
            selectedStyle = MODEL_WOMAN
            changeLookpack(selectedLookpack?.get(currentWomanIndex)!!)
        }

        homeTvMen.setOnClickListener {
            homeTvMen.setTextColor(resources.getColor(R.color.colorAccent))
            homeTvWoman.setTextColor(resources.getColor(R.color.colorPrimaryDark))
            selectedLookpack = menLookpack
            selectedStyle = MODEL_MEN
            changeLookpack(selectedLookpack?.get(currentMenIndex)!!)
        }

        homeTvStyleDetails.setOnClickListener {
            activity!!.drawer_layout.openDrawer(GravityCompat.END)
        }
    }

    private fun changeLookpack(lookpack: Lookpack) {
        val view = Glide.with(context!!).load(lookpack.gambar).transition(DrawableTransitionOptions.with(test1()))
        view.into(homeImgViewStyle)
        homeTvStyleTitle.text = lookpack.namaPack
        fragmentManager!!.beginTransaction()
            .replace(R.id.navigationEnd_frame, DiscoverStyleDetailFragment(lookpack), "").commit()
    }

    private fun callLookEvent() {
        val httpClient = httpClient()
        val apiRequest = retrofitRequest<EventService>(httpClient)
        val loadingFragment = LoadingFragment()
        fragmentManager!!.beginTransaction().add(R.id.frameLayout, loadingFragment).commit()
        call = apiRequest.getLookpack()
        call!!.enqueue(object : Callback<List<LookpackRespone>> {
            override fun onFailure(call: Call<List<LookpackRespone>>, t: Throwable) {
                Log.d("home", "onFailure: ${t.message}")
                if (call.isCanceled) {
                    Toast.makeText(context, "onFailure Canceled", Toast.LENGTH_LONG).show()
                } else {
                    Toast.makeText(context, "Failed fetch data (${t.message})", Toast.LENGTH_LONG).show()
                }
                fragmentManager!!.beginTransaction().remove(loadingFragment).commit()
            }

            override fun onResponse(call: Call<List<LookpackRespone>>, response: Response<List<LookpackRespone>>) {
                response.body()?.forEach {
                    if (it.jenisKelamin == "pria") {
                        Log.d("test", it.namaPack)
                        val lookpack = Lookpack(
                            it.idBarang,
                            it.namaPack,
                            it.deskripsi,
                            decode(it.gambar),
                            it.jenisKelamin
                        )
                        menLookpack!!.add(lookpack)
                    } else if (it.jenisKelamin == "wanita") {
                        val lookpack = Lookpack(
                            it.idBarang,
                            it.namaPack,
                            it.deskripsi,
                            decode(it.gambar),
                            it.jenisKelamin
                        )
                        womanLookpack!!.add(lookpack)
                    }
                }
                if (call.isCanceled) {
                    Toast.makeText(context, "onRespones Canceled", Toast.LENGTH_LONG).show()
                }
                selectedLookpack = womanLookpack
                selectedStyle = MODEL_WOMAN
                changeLookpack(selectedLookpack?.get(currentWomanIndex)!!)
                fragmentManager!!.beginTransaction().remove(loadingFragment).commit()
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onDestroyView() {
        call!!.cancel()
        activity!!.drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, GravityCompat.END)
        super.onDestroyView()
    }

    class test1 : TransitionFactory<Drawable> {
        var crossFadeTransition: DrawableCrossFadeTransition = DrawableCrossFadeTransition(300, true)
        override fun build(dataSource: DataSource?, isFirstResource: Boolean): Transition<Drawable> {
            return crossFadeTransition
        }
    }
}

package Room

import androidx.room.RoomDatabase

abstract class LocalStorageDB : RoomDatabase() {
    abstract fun LocalStorageDao()
}
package Room.DataClass

import androidx.room.ColumnInfo
import androidx.room.Entity

@Entity
data class Env(
    @ColumnInfo(name = "rajaongkir_api_token") var roApiToken: String,
    @ColumnInfo(name = "api_token") var ApiToken: String,
    @ColumnInfo(name = "saved_password") var password: String,
    @ColumnInfo(name = "saved_token") var token: String
)
package Room

import Room.DataClass.Env
import androidx.room.Dao
import androidx.room.Query

@Dao
interface LocalStorageDao {

    @Query("SELECT * FROM Env")
    fun get(): Env

}